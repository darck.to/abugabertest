<?php
    class errorController {
        public function default() {
            include __DIR__ . '/../controller/error/error-default.php';
        }
        public function empty() {
            include __DIR__ . '/../controller/error/error-404.php';
        }
    }
?>