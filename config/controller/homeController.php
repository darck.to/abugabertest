<?php
    class homeController {
        // Main function
        public function home() {
            return Template::home('Test', 'main/main');
        }
        // Template nav
        public function nav() {
            // Carga el menu de acuerdo al usuario
            return Template::render('main/main_nav.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'LOGO_LEFT' => THIS_SERVER . '/assets/img/logoMTY.png',
                'LOGO_RIGHT' => THIS_SERVER . '/assets/img/logo.jpg',
                'HOME' => 'Inicio',
                'HOME_URL' => '/',
                'EVENTS' => 'Eventos',
                'EVENTS_URL' => '/eventos',
                'BLOG' => 'Noticias',
                'BLOG_URL' => '/blog',
                'SCHOOL' => 'Escuela',
                'SCHOOL_URL' => '/escuela',
            ], true);
        }

        // Function main
        public function readApi() {
            // Carga el crud disponible de estudiantes de acuerdo al usuario
            $template = require __DIR__ . '/../controller/main/main-crud.php';
            $template = json_decode($template);
            $data = $template->data;

            return Template::render('main/main_crud.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'ROUTE' => 'main',
                'ADDBUTTON' => 'Agregar Entrada',
                'TH_NOMBRE' => 'Nombre',
                'TH_COMPLETO' => 'Nombre Completo',
                'TH_DESC' => 'Descripción',
                'TH_CREATED' => 'Creado',
                'TH_UPDATED' => 'Actualizado',
                'TH_OWNER' => 'Propietario',
                'TH_AVATAR' => 'Avatar',
                'TH_API' => 'API',
                'TH_ACTIONS' => 'Acciones',
                'DATA' => $data
            ], true);
        }
        // Function create crud for studentes and trainers
        public function create() {
            // Carga el crud disponible de estudiantes de acuerdo al usuario
            $return = require __DIR__ . '/../controller/main/main-create.php';
            return $return;
        }
        // Function read crud for studentes and trainers
        public function read() {
            // Carga el crud disponible de estudiantes de acuerdo al usuario
            $return = require __DIR__ . '/../controller/main/main-read.php';
            return $return;
        }
        // Function update crud for studentes and trainers
        public function update() {
            // Carga el crud disponible de estudiantes de acuerdo al usuario
            $return = require __DIR__ . '/../controller/main/main-update.php';
            return $return;
        }
        // Function delete crud for studentes and trainers
        public function delete() {
            // Carga el crud disponible de estudiantes de acuerdo al usuario
            $return = require __DIR__ . '/../controller/main/main-delete.php';
            return $return;
        }
    }
?>