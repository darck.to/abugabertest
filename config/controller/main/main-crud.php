<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once __DIR__ . '/../../model/_confg/connect.php';
    require_once __DIR__ . '/../../model/_class/main.php';
    
    $database = new DataBaseConnection();
    $db = $database->getConnection();
    
    $entrada = new Main($db);
    
    $data = json_decode(file_get_contents("php://input"));

    // Leemos todas las entradas
    if ($resultOne = $entrada->readApiOne()) {
        $data = $resultOne;
    } if ($resultTwo = $entrada->readApiTwo()) {
        $data = $resultTwo;
    } else {
        http_response_code(503);
        return json_encode(array("message" => "No se encontraron entradas"));
    }
    // Si data true leemos la bdd
    if ($data) {
        if($result = $entrada->read()) {
            $entrada_item = array();
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $background = ($row["src"] == 1) ? "grey-lighter" : "white";
                    $disabled = ($row["src"] == 1) ? "disabled" : "";
                    $icon = ($row["src"] == 1) ? "logo-github" : "balloon-outline";
                    // Por cada owner_login diferente tenemos un color diferente de acuerdo al valor de ownler_login
                    $color = substr($row["owner_login"], 0, 1);                    
                    $entrada_item[] = array("id" => $row["id"], "name" => $row["name"], "full_name" => $row["full_name"], "description" => getWords($row["description"],10), "created_at" => $row["created_at"], "updated_at" => $row["updated_at"], "owner_login" => $row["owner_login"], "owner_avatar_url" => $row["owner_avatar_url"], "API" => $row["src"], "disabled" => $disabled, "background" => $background, "icon" => $icon, "color" => $color,);
                }
            } else {
                $entrada_item[] = array("id" => "", "name" => "", "full_name" => "", "description" => "", "created_at" => "", "updated_at" => "", "owner_login" => "", "owner_avatar_url" => "", "API" => "",);
            }
            http_response_code(201);
            return json_encode(array("message"=> "Se pudo conectar a las APIS", "data" => $entrada_item));
        } else {
            http_response_code(404);
            return json_encode(array("message" => "No se encontraron entradas"));
        }
    } else {
        http_response_code(503);
        return json_encode(array("message" => "No se pudo conectar a las APIS"));
    }    

    // Obtiene las primeras n palabras de la str
	function getWords($sentence, $count) {
		$words = str_word_count($sentence, 1);
        $first_20_words = implode(' ', array_slice($words, 0, $count));
        return $first_20_words;
	}
?>