<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once __DIR__ . '/../../model/_confg/connect.php';
    require_once __DIR__ . '/../../model/_class/main.php';
    
    $database = new DataBaseConnection();
    $db = $database->getConnection();
    
    $entrada = new Main($db);
    
    $data = json_decode(file_get_contents("php://input"));
    // Recibimos el id
    $entrada->id = $data->id;

    // Leemos el contenido de la entrada
    if ($result = $entrada->read()) {
        // Lee result
        $entrada = $result->fetch_assoc();
        http_response_code(200);
        return json_encode(array("message" => "Se leyó la entrada", "data" => $entrada));
    } else {
        http_response_code(503);
        return json_encode(array("message" => "No se encontró la entrada"));
    }
?>