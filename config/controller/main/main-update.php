<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once __DIR__ . '/../../model/_confg/connect.php';
    require_once __DIR__ . '/../../model/_class/main.php';
    
    $database = new DataBaseConnection();
    $db = $database->getConnection();
    
    $entrada = new Main($db);
    
    $data = json_decode(file_get_contents("php://input"));

    // Recibimos los valores de la entrada
    $entrada->id = $data->id;
    $entrada->name = $data->name;
    $entrada->full_name = $data->full_name;
    $entrada->description = $data->description;
    $entrada->created_at = $data->created_at;
    $entrada->updated_at = $data->updated_at;
    $entrada->owner_login = $data->owner_login;
    $entrada->owner_avatar_url = $data->owner_avatar_url;
    $entrada->src = $data->src;
        
    // Leemos todas las entradas
    if ($entrada->update()) {
        http_response_code(200);
        return json_encode(array("message" => "Se editó la entrada"));
    } else {
        http_response_code(503);
        return json_encode(array("message" => "No se editó la entrada"));
    }
?>