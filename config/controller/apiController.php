<?php
    class apiController {
        public function register() {
            include __DIR__ . '/../api/users/register.php';
        }
        public function exist() {
            require __DIR__ . '/../api/users/exist.php';
        }
        public function login() {
            require __DIR__ . '/../api/users/login.php';
        }
        public function read($id) {
            require __DIR__ . '/../api/users/read.php';
            return APIRead::run($id);
        }
        public function update() {
            include __DIR__ . '/../api/users/update.php';
        }
        public function delete() {
            require __DIR__ . '/../api/users/delete.php';
        }
    }
?>