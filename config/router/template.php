<?php
    class Template{
        // Template agrega header y script correspondientes a cada call
        public static function home($title,$script) {
            $head = Template::render('main/headers.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'TITLE' => $title
            ], true);
            $footer = Template::render('main/footer.view.php', [
                'THIS_SERVER' => THIS_SERVER,
            ], true);
            $scripts = Template::render('main/scripts.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'SCRIPT' => $script
            ], true);

            return $head . $footer . $scripts;
        }
        // Template invoca $file, intercambia {{}} por valores de $datos
        public static function render($template, $datos, $caseInsensitive = false ) {
            $file = './config/view/' . $template;
            if (! file_exists($file) ) {
                return 'El archivo <code>' . $file . '</code> es inexistente';
            }
        
            $CI = $caseInsensitive ? 'i' : '';
        
            $contenido = file_get_contents( $file );
            foreach ($datos as $key => $value) {
                // En caso de array lo recorre para insertar varios elementos y no este vacio o null
                if (is_array($value) && !empty($value) && !is_null($value)) {
                    // El nombre del key es el nombre del template a multiplicar
                    $name = strtoupper($key);
                    $cntToMultiply = "";
                    // Busca el inicio y fin del template a multiplicar y lo agrega y reemplaza en la variable contenido
                    $startStr = "{{START_" . $name . "}}";
                    $endStr = "{{END_" . $name . "}}";
                    $startPos = strpos($contenido, $startStr);
                    $endPos = strpos($contenido, $endStr, $startPos + strlen($startStr));
                    foreach ($value as $index => $val) {
                        $cntToMultiply .= substr($contenido, $startPos + strlen($startStr), $endPos - $startPos - strlen($startStr));
                        foreach ($val as $key2 => $value2) {
                            $cntToMultiply = preg_replace("/{{\s*(" . $name. "_" . $key2 . ")\s*?}}/$CI", $value2, $cntToMultiply);
                        }
                    }
                    $contenido = substr_replace($contenido, $cntToMultiply, $startPos, $endPos - $startPos  + strlen($endStr));
                } else {
                    // Si es un solo elemento lo reemplaza directamente
                    $contenido = preg_replace("/{{\s*(" . $key . ")\s*?}}/$CI", $value, $contenido);
                }
            }
            // Si quedaron elementos {{}} sin reemplazar los elimina
            $contenido = preg_replace("/{{\s*([a-zA-Z0-9_]+)\s*?}}/$CI", "", $contenido);
            return $contenido;
        }

        // Template data para profile del usuario
        public static function profile() {
            
        }

        // Template data para entrada del blog
        public static function entrada($title,$script,$entrada) {
            $head = Template::render('main/headers.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'TITLE' => $title
            ], true);
            $footer = Template::render('main/footer.view.php', [
                'THIS_SERVER' => THIS_SERVER,
            ], true);
            $scripts = Template::render('main/scripts.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'SCRIPT' => $script
            ], true);

            return $head . $entrada . $footer . $scripts;
        }
    }
?>