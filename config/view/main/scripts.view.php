<!-- Add your site or application content here -->
<script language="JavaScript" type="text/javascript" src="./js/vendor/dist/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./js/vendor/modernizr-3.11.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./js/plugins.js"></script>
<script src="./css/ionicons/ionicons.esm.js" type="module"></script>
<script nomodule src="./css/ionicons/ionicons.js"></script>

<script src="./js/{{SCRIPT}}.js" type="module"></script>