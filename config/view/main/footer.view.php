<footer class="footer has-background-info">
    <div class="container is-max-desktop">
        <p class="has-text-centered"><small class="has-text-white">Abugaber Test | Todos los derechos reservados 2023</small></p>
        <p class="has-text-centered"><small class="has-text-white">Powered by @mrvg</small></p>
    </div>
</footer>