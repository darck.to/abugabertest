<div class="container is-fluid">
    <div class="columns is-multiline is-justify-content-space-evenly">
        <!--Crea un boton y una table-->
        <div class="column is-full">
            <button class="button is-primary is-small" id="btnCreate" data-route="{{ROUTE}}">{{ADDBUTTON}}</button>
        </div>
        <!--Crea una tabla-->
        <div class="column is-full">
            <table class="table is-fullwidth">
                <thead>
                    <tr>
                        <th>{{TH_NOMBRE}}</th>
                        <th>{{TH_COMPLETO}}</th>
                        <th>{{TH_DESC}}</th>
                        <th>{{TH_CREATED}}</th>
                        <th>{{TH_UPDATED}}</th>
                        <th>{{TH_OWNER}}</th>
                        <th>{{TH_AVATAR}}</th>
                        <th>{{TH_API}}</th>
                        <th>{{TH_ACTIONS}}</th>
                    </tr>
                </thead>
                <tbody id="tableBody">
                    {{START_DATA}}
                    <tr style="background-color: #{{DATA_COLOR}};">
                        <td>{{DATA_NAME}}</td>
                        <td>{{DATA_FULL_NAME}}</td>
                        <td>{{DATA_DESCRIPTION}}</td>
                        <td>{{DATA_CREATED_AT}}</td>
                        <td>{{DATA_UPDATED_AT}}</td>
                        <td>{{DATA_OWNER_LOGIN}}</td>
                        <td>{{DATA_OWNER_AVATAR_URL}}</td>
                        <td><ion-icon name="{{DATA_ICON}}"></ion-icon></td>
                        <td>
                            <button class="button is-warning is-small btnEdit" data-id="{{DATA_ID}}" data-route="{{ROUTE}}" {{DATA_DISABLED}}>Editar</button>
                            <button class="button is-danger is-small btnDelete" data-id="{{DATA_ID}}" data-route="{{ROUTE}}">Borrar</button>
                        </td>
                    </tr>
                    {{END_DATA}}
                </tbody>
            </table>
        </div>
    </div>
</div>