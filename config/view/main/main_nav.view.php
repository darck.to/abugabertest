<div class="has-background-link">
    <div class="container is-max-desktop">
        <nav class="level">
            <a href="{{HOME_URL}}" class="level-item has-text-centered has-background-">
                <div>
                    <span class="heading is-size-6 has-text-white mt-1 mb-1">{{HOME}}</span>
                </div>
            </a>
        </nav>
    </div>
</div>