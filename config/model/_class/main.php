<?php
    class Main {

        private $table = "users_test";
        public $id;
        public $name;
        public $full_name;
        public $description;
        public $created_at;
        public $updated_at;
        public $owner_login;
        public $owner_avatar_url;
        public $src;
        //Connector
        private $conn;

        public function __construct($db) {
            $this->conn = $db;
        }

        // read api
        function readApiOne() {
            $url = 'https://api.github.com/users/PimpTrizkit/repos';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
            if($response = curl_exec($ch)) {
                curl_close($ch);
    
                $data = json_decode($response);
    
                // Save into database only if no id duplicates found
                $stmt = $this->conn->prepare("INSERT INTO `users_test` (`id`, `name`, `full_name`, `description`, `created_at`, `updated_at`, `owner_login`, `owner_avatar_url`, `src`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param("sssssssss", $id, $name, $full_name, $description, $created_at, $updated_at, $owner_login, $owner_avatar_url, $src);
                foreach ($data as $key => $value) {
                    $id = mysqli_real_escape_string($this->conn, $value->id);
                    // Check if id aleady exists
                    $stmtDup = $this->conn->prepare("SELECT id FROM `users_test` WHERE id = ?");
                    $stmtDup->bind_param("s", $id);
                    $stmtDup->execute();
                    $result = $stmtDup->get_result();
                    if ($result->num_rows > 0) {
                        continue;
                    }
                    $name = mysqli_real_escape_string($this->conn, $value->name);
                    $full_name = mysqli_real_escape_string($this->conn, $value->full_name);
                    $description = mysqli_real_escape_string($this->conn, $value->description);
                    $created_at = mysqli_real_escape_string($this->conn, $value->created_at);
                    $updated_at = mysqli_real_escape_string($this->conn, $value->updated_at);
                    $owner_login = mysqli_real_escape_string($this->conn, $value->owner->login);
                    $owner_avatar_url = mysqli_real_escape_string($this->conn, $value->owner->avatar_url);
                    $src = true;
                    $stmt->execute();
                }        
    
                return $data;
            }
            return false;
        }

        // read api two
        function readApiTwo() {
            $url = 'https://api.github.com/users/majimboo/repos';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
            if($response = curl_exec($ch)) {
                curl_close($ch);

                $data = json_decode($response);

                $stmt = $this->conn->prepare("INSERT INTO `users_test` (`id`, `name`, `full_name`, `description`, `created_at`, `updated_at`, `owner_login`, `owner_avatar_url`, `src`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param("sssssssss", $id, $name, $full_name, $description, $created_at, $updated_at, $owner_login, $owner_avatar_url, $src);
                foreach ($data as $key => $value) {
                    $id = mysqli_real_escape_string($this->conn, $value->id);
                    // Check if id aleady exists
                    $stmtDup = $this->conn->prepare("SELECT id FROM `users_test` WHERE id = ?");
                    $stmtDup->bind_param("s", $id);
                    $stmtDup->execute();
                    $result = $stmtDup->get_result();
                    if ($result->num_rows > 0) {
                        continue;
                    }
                    $name = mysqli_real_escape_string($this->conn, $value->name);
                    $full_name = mysqli_real_escape_string($this->conn, $value->full_name);
                    $description = mysqli_real_escape_string($this->conn, $value->description);
                    $created_at = mysqli_real_escape_string($this->conn, $value->created_at);
                    $updated_at = mysqli_real_escape_string($this->conn, $value->updated_at);
                    $owner_login = mysqli_real_escape_string($this->conn, $value->owner->login);
                    $owner_avatar_url = mysqli_real_escape_string($this->conn, $value->owner->avatar_url);
                    $src = true;
                    $stmt->execute();
                }           
                return $data;
            }
            return false;
        }

        // Read from database
        function read() {
            // if id present
            if ($this->id) {
                $query = "SELECT `id`, `name`, `full_name`, `description`, `created_at`, `updated_at`, `owner_login`, `owner_avatar_url`, `src` FROM " . $this->table . " WHERE id = ?";
                $stmt = $this->conn->prepare($query);
                $stmt->bind_param("s", $id);
                $id = mysqli_real_escape_string($this->conn, $this->id);
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    return $result;
                }
            } else {
                $query = "SELECT `id`, `name`, `full_name`, `description`, `created_at`, `updated_at`, `owner_login`, `owner_avatar_url`, `src` FROM " . $this->table  . " ORDER BY `updated_at` DESC";
                $stmt = $this->conn->prepare($query);
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    return $result;
                }
            }
            return false;
        }

        // Create into database
        function create() {
            $query = "INSERT INTO " . $this->table . " SET `id` = ?, `name` = ?, `full_name` = ?, `description` = ?, `created_at` = ?, `updated_at` = ?, `owner_login` = ?, `owner_avatar_url` = ?, `src` = ?";
            $stmt = $this->conn->prepare($query);
            $stmt->bind_param("sssssssss", $id, $name, $full_name, $description, $created_at, $updated_at, $owner_login, $owner_avatar_url, $src);
            $id = mysqli_real_escape_string($this->conn, $this->id);
            $name = mysqli_real_escape_string($this->conn, $this->name);
            $full_name = mysqli_real_escape_string($this->conn, $this->full_name);
            $description = mysqli_real_escape_string($this->conn, $this->description);
            $created_at = mysqli_real_escape_string($this->conn, $this->created_at);
            $updated_at = mysqli_real_escape_string($this->conn, $this->updated_at);
            $owner_login = mysqli_real_escape_string($this->conn, $this->owner_login);
            $owner_avatar_url = mysqli_real_escape_string($this->conn, $this->owner_avatar_url);
            $src = $this->src;
            if ($stmt->execute()) {
                return true;
            }
            return false;
        }

        // Update into datebase
        function update() {
            $query = "UPDATE " . $this->table . " SET `name` = ?, `full_name` = ?, `description` = ?, `created_at` = ?, `updated_at` = ?, `owner_login` = ?, `owner_avatar_url` = ?, `src` = ? WHERE `id` = ?";
            $stmt = $this->conn->prepare($query);
            $stmt->bind_param("sssssssss", $name, $full_name, $description, $created_at, $updated_at, $owner_login, $owner_avatar_url, $src, $id);
            $id = mysqli_real_escape_string($this->conn, $this->id);
            $name = mysqli_real_escape_string($this->conn, $this->name);
            $full_name = mysqli_real_escape_string($this->conn, $this->full_name);
            $description = mysqli_real_escape_string($this->conn, $this->description);
            $created_at = mysqli_real_escape_string($this->conn, $this->created_at);
            $updated_at = mysqli_real_escape_string($this->conn, $this->updated_at);
            $owner_login = mysqli_real_escape_string($this->conn, $this->owner_login);
            $owner_avatar_url = mysqli_real_escape_string($this->conn, $this->owner_avatar_url);
            $src = $this->src;
            if ($stmt->execute()) {
                return true;
            }
            return false;
        }

        // Delete from database
        function delete() {
            $query = "DELETE FROM " . $this->table . " WHERE `id` = ?";
            $stmt = $this->conn->prepare($query);
            $stmt->bind_param("s", $id);
            $id = mysqli_real_escape_string($this->conn, $this->id);
            if ($stmt->execute()) {
                return true;
            }
            return false;
        }

    }
?>