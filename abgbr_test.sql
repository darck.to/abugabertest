-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-08-2023 a las 01:29:16
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `abgbr_test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_test`
--

CREATE TABLE `users_test` (
  `id` int(11) NOT NULL COMMENT 'id de la table',
  `name` text DEFAULT NULL COMMENT 'name str nombre',
  `full_name` text DEFAULT NULL COMMENT 'fullname str fn',
  `description` text DEFAULT NULL COMMENT 'dcrpt str',
  `created_at` datetime DEFAULT NULL COMMENT 'creado cuando dt',
  `updated_at` datetime DEFAULT NULL COMMENT 'actualizado cuando dt',
  `owner_login` text DEFAULT NULL COMMENT 'owner login ?',
  `owner_avatar_url` text DEFAULT NULL COMMENT 'owner url',
  `src` int(1) NOT NULL COMMENT '0 for api\r\n1 for system'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `users_test`
--

INSERT INTO `users_test` (`id`, `name`, `full_name`, `description`, `created_at`, `updated_at`, `owner_login`, `owner_avatar_url`, `src`) VALUES
(17535387, 'thumborizer', 'majimboo/thumborizer', 'A image processing server in node', '2014-03-08 04:52:22', '2017-07-06 04:15:10', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(19306876, 'tapsilog', 'majimboo/tapsilog', 'an asynchronous logging service', '2014-04-30 08:33:11', '2014-06-09 07:25:36', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(22129727, 'nexy', 'majimboo/nexy', 'Nexy is a middleware based TCP framework for Node', '2014-07-23 03:02:21', '2017-10-28 11:41:15', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(22260949, 'stream-frame', 'majimboo/stream-frame', 'a stream framing library for node.js', '2014-07-25 15:08:10', '2023-06-08 14:51:22', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(22294335, 'c-struct', 'majimboo/c-struct', 'a binary data packing & unpacking library for node.js', '2014-07-26 19:16:06', '2023-04-15 00:34:50', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(22349206, 'majimboo.github.io', 'majimboo/majimboo.github.io', NULL, '2014-07-28 16:13:28', '2017-07-06 04:15:20', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(22465569, 'roBrowser', 'majimboo/roBrowser', 'roBrowser is a free and open-source implementation of the Ragnarok Online MMORPG for web browsers written from scratch using the latest web standards (WebGL, HTML5, File API, Javascript, Threads, ...).', '2014-07-31 10:31:18', '2018-02-13 23:53:53', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(22672492, 'winston-couchbase', 'majimboo/winston-couchbase', 'A couchbase transport for winston.', '2014-08-06 06:43:38', '2018-01-03 06:03:46', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(22830736, 'node-benchmarks', 'majimboo/node-benchmarks', 'Benchmarking various operations in Node.JS', '2014-08-11 07:34:21', '2023-04-13 14:26:29', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(22865135, 'node', 'majimboo/node', 'evented I/O for v8 javascript', '2014-08-12 04:49:56', '2014-08-12 05:11:09', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(23061789, 'node-fast-buffer', 'majimboo/node-fast-buffer', 'A faster way of handling buffers.', '2014-08-18 07:00:42', '2014-10-03 03:49:04', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(23196786, 'kamote', 'majimboo/kamote', 'Yet another RPC for Node.', '2014-08-21 17:41:41', '2021-08-15 00:27:32', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(24083017, 'weebchat', 'majimboo/weebchat', 'Dynamically Distributed Telnet Chat Server', '2014-09-16 02:55:29', '2016-08-30 23:01:49', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(24496260, 'node-multistore', 'majimboo/node-multistore', 'For Marco Babes (re)Viewing Pleasure', '2014-09-26 10:53:51', '2014-10-20 15:57:46', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(24547023, 'ensemble', 'majimboo/ensemble', 'Simple, Elegant Discussion Board', '2014-09-28 01:33:24', '2014-09-28 01:35:23', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(24577916, 'notes', 'majimboo/notes', 'Just Learning Stuff', '2014-09-29 02:59:55', '2014-09-29 04:21:29', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(30377059, 'buffer-crc32', 'majimboo/buffer-crc32', 'A pure javascript CRC32 algorithm that plays nice with binary data', '2015-02-05 20:35:44', '2015-02-09 22:05:42', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(30586831, 'reloadjs', 'majimboo/reloadjs', 'Just another hotload module', '2015-02-10 10:22:58', '2019-03-03 05:45:46', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(44723879, 'snappy', 'majimboo/snappy', 'The Rockettoise', '2015-10-22 05:23:10', '2015-10-22 06:04:44', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(46245698, 'meteor-test-project', 'majimboo/meteor-test-project', 'snapzio', '2015-11-16 01:55:53', '2015-11-16 16:23:30', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(56774487, 'login-checker', 'majimboo/login-checker', NULL, '2016-04-21 13:06:33', '2020-09-20 16:45:18', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(73869851, 'py-mathutils', 'majimboo/py-mathutils', 'A fix of https://pypi.python.org/pypi/mathutils', '2016-11-16 01:05:58', '2023-04-17 08:51:01', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(74562321, 'playcanvas-engine', 'majimboo/playcanvas-engine', 'JavaScript game engine built on WebGL and WebVR.', '2016-11-23 09:41:51', '2016-11-23 10:03:20', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(81162069, 'logrus', 'majimboo/logrus', 'Structured, pluggable logging for Go.', '2017-02-07 03:27:34', '2017-02-07 03:27:36', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(91309689, 'mviewer', 'majimboo/mviewer', 'Reverse Engineer MView 3D File Format', '2017-05-15 07:49:21', '2023-08-05 17:12:20', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(98222196, 'khxn-xttxck', 'majimboo/khxn-xttxck', 'for Erwin with love...', '2017-07-24 18:30:13', '2017-07-25 03:38:12', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(120158234, 'i2p.i2p', 'PimpTrizkit/i2p.i2p', 'My purpose here is to get my feet wet with i2pSnark. I\'m not trying to do anything real here... just yet. I\'m just seeing how interested I will be in working with i2pSnark. -------- I2P is an anonymizing network, offering a simple layer that identity-sensitive applications can use to securely communicate. All data is wrapped with several layers of encryption, and the network is both distributed and dynamic, with no trusted parties. This is a mirror of the official Monotone repository.', '2018-02-04 05:36:41', '2018-02-14 01:04:05', 'PimpTrizkit', 'https://avatars.githubusercontent.com/u/12415937?v=4', 1),
(124007217, 'PJs', 'PimpTrizkit/PJs', 'Pimped Javascript - Just a library of my javascript functions, or shims. They are generally built for speed, size, versatility, and portability (copy and paste-able). Readability will be sacrificed. Because they are optimized for usage right out-of-the-box. ', '2018-03-06 02:16:21', '2023-08-08 22:11:36', 'PimpTrizkit', 'https://avatars.githubusercontent.com/u/12415937?v=4', 1),
(199174878, 'khan-3rdparty', 'majimboo/khan-3rdparty', NULL, '2019-07-27 14:27:23', '2022-06-24 12:08:43', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(222126115, 'binpack', 'majimboo/binpack', NULL, '2019-11-16 16:18:09', '2019-11-16 16:18:26', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(270338106, 'fb_createdate_approximator', 'majimboo/fb_createdate_approximator', 'Approximate the date a facebook account was created', '2020-06-07 14:53:34', '2023-07-18 14:04:51', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(401290088, 'majimboo', 'majimboo/majimboo', NULL, '2021-08-30 09:39:13', '2021-08-30 09:46:23', 'majimboo', 'https://avatars.githubusercontent.com/u/6186420?v=4', 1),
(401290093, 'Roberto Valdez', 'BBETO', 'asdfasdf', '2023-08-17 12:12:00', '2023-08-17 13:31:00', 'dfasd', 'fasdf', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users_test`
--
ALTER TABLE `users_test`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users_test`
--
ALTER TABLE `users_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la table', AUTO_INCREMENT=401290094;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
