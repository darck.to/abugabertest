const host = window.location.protocol + '//' + window.location.host;

// Valuda inputs
export function validaInput(input) {
    let returnValue = true;
    $.each($(input), function() {
        if ($(this).val() == '') {
            $(this).next().removeClass('is-hidden');
            returnValue = false;
        } else {
            $(this).next().addClass('is-hidden');
        }
    });
    console.log(returnValue);
    return returnValue;
}

// Show and hide columnOne and columnTwo
export function toggleColumnsCrud() {
    $('#columnOne').toggleClass('is-hidden');
    $('#columnTwo').toggleClass('is-hidden');
    $('#EscuelaCRUD').html('');
}