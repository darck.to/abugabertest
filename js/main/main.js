import * as crud from './main-crud.js';

$(function() {
    'use strict';
    // Obtenemos host
    const host = window.location.protocol + '//' + window.location.host;

    $('#MainContainer').load(host + '/templates/main/main.html', ()=>{
        // Cargamos mediante post el template de columna izquierda
        $.ajax({
            url: host + '/home/readApi/',
            contentType: 'application/json',
            type: 'POST',
            dataType: 'html',
            success: function(response, textStatus, xhr) {
                $('#MainColumnSection').html(response);
                // Cargamos mediante
                $('#btnCreate, .btnEdit, .btnDelete').on('click', function(e) {
                    e.preventDefault();
                    const id = $(this).attr('data-id');
                    const way = $(this).attr('data-route');
                    let action = '';
                  
                    // Determine the action based on the button clicked
                    if ($(this).hasClass('btnEdit')) {
                        action = 'update';
                    } else if ($(this).hasClass('btnDelete')) {
                        if (!confirm('Desea eliminar el registro?')) {
                            return;
                        }
                        action = 'delete';
                    } else {
                        action = 'create';
                    }
                  
                    crud.createCrud(action, way, id);
                });
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
            }
        });
    });

    
    // Cargamos mediante post el template de navegacion principal
    $.ajax({
        url: host + '/home/nav/',
        contentType: 'application/json',
        type: 'POST',
        dataType: 'html',
        success: function(response, textStatus, xhr) {
            $('#MainNavigator').html(response);
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log(xhr.responseText);
        }
    });

}());