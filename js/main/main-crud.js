import * as utils from './main-utils.js';

const host = window.location.protocol + '//' + window.location.host;
// Function createCrud
export function createCrud(action,route,id) {
    const crudName = (route == 'home') ? 'Main' : '';
    // Action
    const actionName = (action == 'create') ? 'Crear' : 'Editar';
    // Si action == edit carga datos de usuario desde BD
    if (action == 'update') {
        function getUserData(id) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: host + '/home/read/',
                    // Content type sent to server application/json
                    contentType: 'application/json',
                    type: 'POST',
                    dataType: 'html',
                    data: JSON.stringify({
                        id : id
                    }),
                    success: function(response, textStatus, xhr) {
                        // Lee el objeto response y obtiene data
                        const userdata = JSON.parse(response);
                        resolve(userdata.data);
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        console.log(xhr.responseText);
                        return false;
                    }
                });
            });
        }
        getUserData(id).then(function(response) {
            cargaFormCrud(response, response.id);
        }).catch(function(error) {
            console.log(error);
            return false;
        });
    } else if (action == 'delete') { // Si action == delete elimina usuario desde BD
        const routeName = (route == 'main') ? 'Main' : '';
        $.ajax({
            url: host + '/home/delete/' + route,
            // Content type sent to server application/json
            contentType: 'application/json',
            type: 'POST',
            dataType: 'html',
            data: JSON.stringify({
                id : id
            }),
            success: function(response, textStatus, xhr) {
                console.log(response);
                // Reload window
                window.location.reload();
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
                return false;
            }
        });
        return false;
    } else { // Si action == create carga formulario vacio
        let emptyuser = {'name':'', 'full_name':'', 'description':'', 'created_at':'', 'updated_at':'', 'owner_login':'', 'owner_avatar_url':'', 'src':''};
        cargaFormCrud(emptyuser);
    } // End if action == edit
    function cargaFormCrud(user,userid) {
        utils.toggleColumnsCrud();
        const disabledProp = (user.src === 1) ? 'disabled' : '';
        const background = (user.src === 1) ? 'background-color: #f5f5f5;' : '';
        const createForm = `
            <div class="column">
                <div class="card">
                    <div class="card-header">
                        <div class="card-header-title">${actionName} ${crudName}</div>
                        <div class="card-header-icon">
                            <span class="icon is-pulled-right is-clickable has-text-warning-dark closeCrud"><ion-icon class="is-size-2" size="large" name="close-circle"></ion-icon></span></div></div>
                    <div class="card-content">
                        <div class="field">
                            <div class="field">
                                <label>Nombre</label>
                                <input class="input" id="nameT" type="text" value="${user.name}"></div>
                            <div class="field">
                                <label>Nombre Completo</label>
                                <input class="input" id="fullT" type="text" value="${user.full_name}"></div>
                            <div class="field">
                                <label>Descripcion</label>
                                <input class="input" id="descT" type="text" value="${user.description}"></div>
                            <div class="field">
                                <label>Creado En</label>
                                <input class="input" id="creaT" type="datetime-local" value="${user.created_at}T00:00"></div>
                            <div class="field">
                                <label>Actualizado En</label>
                                <input class="input" id="updaT" type="datetime-local" value="${user.updated_at}T00:00"></div>
                            <div class="field">
                                <label>Login De</label>
                                <input class="input" id="owneT" type="text" value="${user.owner_login}"></div>
                            <div class="field">
                                <label>Avatar URL</label>
                                <input class="input" id="urlT" type="text" value="${user.owner_avatar_url}"></div>
                            <button id="CrudCreate" class="button is-link" data-action="${action}" data-route="${route}" ${disabledProp}>${actionName}</button></div></div></div></div></div></div></div>
        `;
        $('#MainCRUD').html(createForm);
        // Cierra CRUD
        $('.closeCrud').on('click', function() {
            utils.toggleColumnsCrud();
        });
        // Guarda en el valor de route
        $('#CrudCreate').on('click', function() {
            // Validate inputs
            if (!utils.validaInput('.input-advice')) {
                return false;
            }
            // This Data-Route
            const route = $(this).data('route');
            const routeName = (route == 'students') ? 'estudiantes' : 'entrenadores';
            // This Data-Action
            const action = $(this).data('action');
            // formData todos los inputs
            const formData = {
                id: (!userid) ? 'null' : userid,
                name: $('#nameT').val(),
                full_name: $('#fullT').val(),
                description: $('#descT').val(),
                created_at: $('#creaT').val(),
                updated_at: $('#updaT').val(),
                owner_login: $('#owneT').val(),
                owner_avatar_url: $('#urlT').val(),
                src: 0
            };
            // post to /escuela/route
            $.ajax({
                url: host + '/home/' + action + '/' + route,
                // Content type sent to server application/json
                contentType: 'application/json',
                type: 'POST',
                dataType: 'html',
                data: JSON.stringify(formData),
                success: function(response, textStatus, xhr) {
                    $('#EscuelaCRUD').html('');
                    utils.toggleColumnsCrud();
                    // Reload window
                    window.location.reload();
                },
                error: function(xhr, textStatus, errorThrown) {
                    console.log(xhr.responseText);
                }
            });
        });
    } // End function cargaFormCrud
} // End function createCrud