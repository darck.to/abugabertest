<?php
  require_once __DIR__ . './config/router/template.php';
  require_once __DIR__ . './config/router/config.php';
  require_once __DIR__ . './config/router/router.php';

  $router = new Router();

  echo $router->run();
?>